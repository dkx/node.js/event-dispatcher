import 'reflect-metadata';
import {expect} from 'chai';

import {Event, EventDispatcher, Subscribe, Subscriber} from '../../src';


let events: EventDispatcher;


describe('#EventDispatcher', () => {

	beforeEach(() => {
		events = new EventDispatcher;
	});

	describe('dispatch()', () => {

		it('should throw an error when subscription method is missing an argument', () => {
			expect(() => {
				class TestSubscriber implements Subscriber
				{
					@Subscribe()
					public onEvent(): void {}
				}
			}).to.throw(Error, 'Subscription TestSubscriber.onEvent contains 0 arguments, but it must have exactly one');
		});

		it('should throw an error when subscription method argument is missing a type', () => {
			expect(() => {
				class TestSubscriber implements Subscriber
				{
					@Subscribe()
					public onEvent(event): void {}
				}
			}).to.throw(Error, 'Subscription TestSubscriber.onEvent is missing an event type in it\'s argument');
		});

		it('should dispatch event and call subscribers', async () => {
			let called: Array<string> = [];

			class TestEvent implements Event {}
			class TestSubscriber implements Subscriber
			{
				@Subscribe()
				public onEventA(event: TestEvent): void
				{
					expect(event).to.be.an.instanceOf(TestEvent);
					called.push('a');
				}

				@Subscribe()
				public onEventB(event: TestEvent): void
				{
					expect(event).to.be.an.instanceOf(TestEvent);
					called.push('b');
				}
			}

			events.addSubscriber(new TestSubscriber);
			await events.dispatch(new TestEvent);

			expect(called).to.have.members(['a', 'b']);
		});

	});

});
