# DKX/EventDispatcher

Event dispatcher for node.js

## Installation

**Dependencies:**

* `reflect-metadata`

with npm:

```bash
$ npm install --save @dkx/event-dispatcher
```

or with yarn:

```bash
$ yarn add @dkx/event-dispatcher
```

## Events

Events should be small value object classes implementing the `Event` interface.

```typescript
import {Event} from '@dkx/event-dispatcher';

class UserUpdatedEvent implements Event
{
    constructor(
        public readonly user: User,
        public readonly changes: any,
    ) {}
}
```

## Subscribers

Subscribers are classes which contains subscriptions to defined events. They should implement the `Subscriber` interface.

```typescript
import {Subscriber, Subscribe} from '@dkx/event-dispatcher';
import {UserUpdatedEvent} from './user-updated-event';

class UserUpdatedEmailNotification implements Subscriber
{
    @Subscribe()
    public onUserUpdated(event: UserUpdatedEvent): void
    {
        console.log(event);
        // todo send email
    }
}
```

Event to which the subscriber should subscribe is taken from method argument type (`event: UserUpdatedEvent`).

Subscription method can be also `async` and return `Promise`.

## Call event

```typescript
import {EventDispatcher} from '@dkx/event-dispatcher';
import {UserUpdatedEvent} from './user-updated-event';
import {UserUpdatedEmailNotification} from './user-updated-email-notification';

// create and register first subscriber
const events = new EventDispatcher;
events.addSubscriber(new UserUpdatedEmailNotification);

// dispatch event
events.dispatch(new UserUpdatedEvent(user, {}));
```
