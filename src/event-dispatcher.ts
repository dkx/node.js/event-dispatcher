import {ClassType} from '@dkx/types-class';

import {Subscriber} from './subscriber';
import {Event} from './event';
import {SUBSCRIPTIONS_METADATA, SubscriberSubscriptions} from './decorators';


declare interface Subscription
{
	subscriber: Subscriber,
	method: string,
}


export class EventDispatcher
{


	private subscriptions: WeakMap<ClassType<Event>, Array<Subscription>> = new WeakMap;


	public addSubscriber(subscriber: Subscriber): void
	{
		const subscriptions: SubscriberSubscriptions = Reflect.getMetadata(SUBSCRIPTIONS_METADATA, subscriber);
		if (typeof subscriptions === 'undefined') {
			return;
		}

		for (let subscription of subscriptions) {
			let realSubscriptions: Array<Subscription>;

			if (!this.subscriptions.has(subscription.eventType)) {
				realSubscriptions = [];
				this.subscriptions.set(subscription.eventType, realSubscriptions);
			} else {
				realSubscriptions = this.subscriptions.get(subscription.eventType);
			}

			realSubscriptions.push({
				subscriber,
				method: subscription.method,
			});
		}
	}


	public async dispatch(event: Event): Promise<void>
	{
		const proto = Object.getPrototypeOf(event).constructor;

		if (!this.subscriptions.has(proto)) {
			return;
		}

		const subscriptions = this.subscriptions.get(proto);
		const promises = subscriptions.map((subscription) => {
			return Promise.resolve(subscription.subscriber[subscription.method](event));
		});

		await Promise.all(promises);
	}

}
