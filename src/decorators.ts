import {ClassType} from '@dkx/types-class';

import {Event} from './event';


export const SUBSCRIPTIONS_METADATA: string = 'event-dispatcher:subscriptions';
export declare type SubscriberSubscriptions = Array<{method: string, eventType: ClassType<Event>}>;


export function Subscribe(): MethodDecorator
{
	return function(target: Object, property: string, descriptor: TypedPropertyDescriptor<any>): TypedPropertyDescriptor<any>
	{
		const argumentTypes: Array<any> = Reflect.getOwnMetadata('design:paramtypes', target, property);
		if (argumentTypes.length !== 1) {
			throw new Error(`Subscription ${target.constructor.name}.${property} contains ${argumentTypes.length} arguments, but it must have exactly one`);
		}

		const eventType = argumentTypes[0];
		if (eventType === Object) {
			throw new Error(`Subscription ${target.constructor.name}.${property} is missing an event type in it's argument`);
		}

		const subscriptionsList: SubscriberSubscriptions = Reflect.getMetadata(SUBSCRIPTIONS_METADATA, target) || [];
		subscriptionsList.push({
			method: property,
			eventType,
		});

		Reflect.defineMetadata(SUBSCRIPTIONS_METADATA, subscriptionsList, target);

		return descriptor;
	}
}